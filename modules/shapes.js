const drawCircle_ = (s,{radius}) => {
    let {arrow: {position: {x,y}, width, colour: {r,g,b,a}},vctx} = s;
    vctx.beginPath();
    vctx.strokeStyle = `rgba(${r},${g},${b},${a})`;
    vctx.lineWidth = width;
    vctx.arc(x, y, radius, 0, 2 * Math.PI);
    vctx.stroke();
}
const drawEllipse_ = (s,{radiusY,radiusX}) => {
    let {arrow: {position: {x,y}, width,angle, colour: {r,g,b,a}},vctx} = s;
    vctx.save();
    vctx.translate(x, y);
    vctx.rotate(angle);
    vctx.translate(-x, -y);
    vctx.beginPath();
    vctx.strokeStyle = `rgba(${r},${g},${b},${a})`;
    vctx.lineWidth = width;
    vctx.ellipse(x, y, radiusX, radiusY, 0, 0, 2 * Math.PI);
    vctx.stroke();
    vctx.restore();
}
const drawRectangle_ = (s,{dX,dY}) => {
    let {arrow: {position: {x,y}, width,angle, colour: {r,g,b,a}},vctx} = s;
    vctx.save();
    vctx.translate(x, y);
    vctx.rotate(angle);
    vctx.translate(-x, -y);
    vctx.beginPath();
    vctx.strokeStyle = `rgba(${r},${g},${b},${a})`;
    vctx.lineWidth = width;
    vctx.rect(x-dX, y-dY, dX*2, dY*2);
    vctx.stroke();
    vctx.restore();
}

const drawEmoji_ = (s, {code,radius}) => {
    radius = radius ? radius : 15;
    let {resources, arrow: {position: {x,y}, width,angle, colour: {r,g,b,a}},vctx} = s;
    let im = resources[code];
    vctx.save();
    vctx.translate(x, y);
    vctx.transform(1,0,0,-1,0,0);
    vctx.rotate(angle);

    vctx.translate(-x, -y);
    vctx.drawImage(im,x-radius, y-radius, radius*2, radius*2);
    vctx.restore();

}


const clear_ = ({vctx},args) => {
    vctx.clearRect(-350,-350,700,700);
}



export {drawCircle_, drawEllipse_, drawRectangle_,drawEmoji_, clear_};