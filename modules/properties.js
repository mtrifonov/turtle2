import RGBColor from './colour_parser.js';

const setColour_ = ({arrow},{r,g,b,a}) => {
    if (typeof r === "string") {
        let arr = new RGBColor(r).toRGB_array();
        arrow.colour.r = arr[0];
        arrow.colour.g = arr[1];
        arrow.colour.b = arr[2];
        arrow.colour.a = 1;
    } else if (Array.isArray(r)) {
        let arr = r;
        arrow.colour.r = arr[0];
        arrow.colour.g = arr[1];
        arrow.colour.b = arr[2];
        arrow.colour.a = 1;
    } else {
        arrow.colour.r = r;
        arrow.colour.g = g;
        arrow.colour.b = b;
        arrow.colour.a = a ? a : 1; 
    }
};
const setWidth_ = ({arrow},{width}) => {
    arrow.width = width;
    //console.log(arrow);
}

const showCoords_ = ({arrow}) => {arrow.coords = true};

const reset_ = ({arrow}) => {
    arrow.position =  {x: 0, y: 0};
    arrow.angle = 0;
    arrow.width = 1;
    arrow.colour = {r: 0, g: 100, b: 0, a:1};
}

const hide_ = ({arrow}) => {
    arrow.visible = false;
}

export {setColour_, setWidth_, reset_, hide_,showCoords_};