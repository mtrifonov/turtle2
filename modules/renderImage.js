import {penDown_, penUp_, moveTo_, forward_, backward_, left_, right_} from './lines_and_movement.js';
import {drawCircle_, drawEmoji_, drawRectangle_, drawEllipse_, clear_} from './shapes.js';
import {setColour_,setWidth_,reset_,hide_,showCoords_} from './properties.js';
import fill_ from './fill.js';
import template_ from './template.js';
import drawCoords from './coords.js';

const methods = {
    penDown: penDown_,
    penUp: penUp_,
    drawCircle: drawCircle_,
    drawEllipse: drawEllipse_,
    drawRectangle: drawRectangle_,
    drawEmoji: drawEmoji_,
    setColour: setColour_,
    setWidth: setWidth_,
    moveTo: moveTo_,
    forward: forward_,
    backward: backward_,
    left: left_,
    right: right_,
    fill: fill_,
    reset: reset_,
    hide: hide_,
    template: template_,
    showCoords: showCoords_,
    clear: clear_,
}

const render = (state,actionQueue) => {
    detectPathSequences(actionQueue);
    //console.log(actionQueue);
    loadResources(state).then(_ =>{
    for (let i = 0; i < actionQueue.length; i++) {
        let action = actionQueue[i];
        methods[action.type](state,action.args);
    }
    renderCTX(state);
    });
    state.renderScheduled = false;
}

const loadResources = async({resourcesToLoad,resources}) => {
    let arr = [];
    for (let i = 0; i < resourcesToLoad.length; i++) {
        let code = resourcesToLoad[i];
        // let image = await fetch(`https://mtrifonov.gitlab.io/turtle/openmoji-svg-color/${code}.svg`);
        // image = await image.blob();
        arr.push(new Promise((resolve, reject) => {
            let im = document.createElement('img');
            im.crossOrigin = "Anonymous";
            im.src = `https://storage.googleapis.com/resources-programming-level-1/openmoji-svg-color/${code}.svg`;
            resources[code] = im;
            im.onload = () => {resolve()};
        }));
    }
    await Promise.all(arr);
}


const detectPathSequences = (actionQueue) => {
    const isOneOf = (item,a) => {
        for (let i = 0; i < a.length; i++) {
            if (item === a[i]) return true;
        } 
        return false;
    }
    let pathToggle = false;
    let index = 0;
    while (index < actionQueue.length) {
        //console.log(index,[...actionQueue]);
        const action = actionQueue[index];
        //console.log(action);
        if (pathToggle) {
            if (action.type === "penDown") {
                actionQueue.splice(index,1);
                index--;
            } else if (action.type === "penUp") {
                pathToggle = false;
            } else if (!isOneOf(action.type,["moveTo","forward","left","right","backward","setWidth","setColour"])) {
                pathToggle = false;
                //console.log(index);
                actionQueue.splice(index,0,{type:"penUp",args:null});
                index++;
            }
        } else {
            if (action.type === "penDown") {
                pathToggle = true;
            } else if (action.type === "penUp") {
                actionQueue.splice(index,1);
                index--;
            } 
        }
        index++;
    }
    //console.log(pathToggle);
    if (pathToggle) {
        actionQueue.push({type:"penUp",args:null});
    }
}

const renderCTX = ({ctx,virtualCanvas,canvas,arrow,coordLine}) => {
    ctx.clearRect(-canvas.width / 2, -canvas.height / 2, canvas.width, canvas.height);
    if (arrow.coords) drawCoords(ctx);
    ctx.save();
    ctx.setTransform(1,0,0,1,0,0);
    ctx.drawImage(virtualCanvas,0,0, canvas.width, canvas.height);
    ctx.restore();
    if (arrow.visible) drawArrow(arrow,ctx);
}

const drawArrow = (arrow,ctx) => {
    var x = arrow.position.x;
    var y = arrow.position.y;
    //console.log(x,y);
    var w = 10;
    var h = 15;
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(-arrow.angle);
    ctx.translate(-x, -y);
    ctx.beginPath();
    ctx.moveTo(x - w / 2, y);
    ctx.lineTo(x + w / 2, y);
    ctx.lineTo(x, y + h);
    ctx.closePath();
    ctx.fillStyle = `rgba(${arrow.colour.r},${arrow.colour.g},${arrow.colour.b},${arrow.colour.a})`;
    ctx.fill();
    ctx.restore();
}

export { render };