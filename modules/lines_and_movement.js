const penDown_ = ({arrow,vctx}) => {
    arrow.penDown = true;
    arrow.penDownCoords = {x:arrow.position.x,y:arrow.position.y};
    vctx.beginPath();
    vctx.moveTo(arrow.position.x, arrow.position.y);
}

const closeEnough = ({x:x1,y:y1},{x:x2,y:y2}) => {
    let dist = Math.sqrt((x1-x2)**2 + (y1-y2)**2);
    return dist < 10;
}

const penUp_ = ({arrow,vctx}) => {
    let {colour: {r,g,b,a},width} = arrow;
    arrow.penDown = false;
    vctx.strokeStyle = `rgba(${r},${g},${b},${a})`;
    vctx.lineWidth = width;
    if (closeEnough(arrow.position,arrow.penDownCoords)) { 
        vctx.closePath();
    };
    arrow.penDownCoords = undefined;
    vctx.stroke();
}


const forward_ = (state,{dist}) => {
    let {arrow: {angle, position: {x,y}}} = state;
    let y_delta = Math.cos(angle) * dist;
    let x_delta = Math.sin(angle) * dist;
    x = x + x_delta;
    y = y + y_delta;
    moveTo_(state,{x,y});
}

const backward_ = (state,{dist}) => {
    forward_(state,{dist:-dist});
}

const moveTo_ = ({arrow,vctx},{x,y}) => {
    arrow.position.x = x;
    arrow.position.y = y;
    if (arrow.penDown) {
        vctx.lineTo(x,y);
        //console.log(arrow);
    } 
}

const right_ = ({arrow,vctx},{deg}) => {
    deg %= 360;
    let rad = Math.PI * 2 * (deg / 360);
    arrow.angle += rad;
}

const left_ = (s,{deg}) => {
    right_(s,{deg:-deg});
}

export {penDown_, penUp_, forward_, backward_, moveTo_, right_, left_};