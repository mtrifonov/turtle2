import { render } from './modules/renderImage.js';
import displayCoordinates from './modules/display_coordinates.js';

const state = {
    canvas: document.getElementById('paintcanvas'),
    virtualCanvas: document.createElement('canvas'),
    renderScheduled: false,
    arrow:{
        position: {x: 0, y: 0},
        angle: 0,
        width: 1,
        penDown: false,
        visible: true,
        colour: {r: 0, g: 100, b: 0, a:1},
        coords: false,
    },
    resourcesToLoad: [],
    resources: {},
    init: function() {
        this.virtualCanvas.width = this.canvas.width;
        this.virtualCanvas.height = this.canvas.height;
        this.ctx = this.canvas.getContext('2d');
        this.vctx = this.virtualCanvas.getContext('2d');
        this.vctx.setTransform(1, 0, 0, -1,350,350);
        this.ctx.setTransform(1, 0, 0, -1,350,350);
    },
};
state.init();

const actionQueue = {
    data: [],
    add: function(action) {
        if (!state.renderScheduled) {
            state.renderScheduled = true;
            setTimeout(() => {
                render(state,[...this.data]);
                // experimental:
                this.data = [];
            },0);
        };
        if (action.resource) {
            state.resourcesToLoad.push(action.resource);
        };
        Object.freeze(action);
        this.data.push(action);
    }, 
    init: function() {
        this.add({type: "reset",args: null});
        displayCoordinates(state);
    },
};
actionQueue.init();

const drawCircle = (radius) => actionQueue.add({type: "drawCircle", args: {radius: radius}});
const drawEllipse = (radiusX,radiusY) => actionQueue.add({type: "drawEllipse", args: {radiusX: radiusX,radiusY: radiusY}});
const drawRectangle = (dX,dY) => actionQueue.add({type: "drawRectangle", args: {dX: dX,dY: dY}});
const penUp = () => actionQueue.add({type: "penUp", args: null});
const penDown = () => actionQueue.add({type: "penDown", args: null});
const moveTo = (x,y) => actionQueue.add({type: "moveTo", args: {x: x, y: y}});
const forward = (dist) => actionQueue.add({type: "forward", args: {dist: dist}});
const backward = (dist) => actionQueue.add({type: "backward", args: {dist: dist}});
const left = (deg) => actionQueue.add({type: "left", args: {deg:deg}});
const right = (deg) => actionQueue.add({type: "right", args: {deg: deg}});
const fill = () => actionQueue.add({type: "fill", args: null});
const setWidth = (width) => actionQueue.add({type: "setWidth", args: {width:width}});
const setColour = (r,g,b,a) => actionQueue.add({type: "setColour", args: {r:r,g:g,b:b,a}});
const drawEmoji = (code,radius) => actionQueue.add({type: "drawEmoji",args:{code:code,radius:radius},resource:code});
const reset = () => actionQueue.add({type: "reset",args: null});
const hide = () => actionQueue.add({type: "hide",args:null});
const showCoords = () => actionQueue.add({type: "showCoords",args:null});
const template = (id) => actionQueue.add({type: "template",args:{id:id}});
const clear = () => actionQueue.add({type: "clear",args:null});


window.showCoords = showCoords;
window.drawCircle = drawCircle;
window.drawEllipse = drawEllipse;
window.drawRectangle = drawRectangle;
window.penDown = penDown;
window.penUp = penUp;
window.goTo = moveTo;
window.forward = forward;
window.left = left;
window.right = right;
window.fill = fill;
window.setColour = setColour;
window.setWidth = setWidth;
window.drawEmoji = drawEmoji;
window.reset = reset;
window.hide = hide;
window.template = template;
window.backward = backward;
window.clear = clear;

//export { fill, showCoords, reset, template, hide, penUp, penDown, drawCircle, left, right, setWidth, setColour, drawRectangle, drawEmoji, drawEllipse, moveTo, forward };